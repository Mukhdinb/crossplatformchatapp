// Firebase App (the core Firebase SDK) is always required and must be listed first
import firebase from "firebase/app";
// If you are using v7 or any earlier version of the JS SDK, you should import firebase using namespace import
// import * as firebase from "firebase/app"


// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/database";

// Your web app's Firebase configuration
  var firebaseConfig = {
      apiKey: "AIzaSyAmyBBE9r0fR6o1VWZNyc0mEmNMy63gvw8",
     authDomain: "waychat-c6b73.firebaseapp.com",
     databaseURL: "https://waychat-c6b73-default-rtdb.europe-west1.firebasedatabase.app",
     projectId: "waychat-c6b73",
     storageBucket: "waychat-c6b73.appspot.com",
     messagingSenderId: "805727148317",
     appId: "1:805727148317:web:2fdb1b2ac637e20b1d69f4"
  };
  // Initialize Firebase
  let firebaseApp = firebase.initializeApp(firebaseConfig);
  let firebaseAuth= firebaseApp.auth()
  let firebaseDB = firebaseApp.database()

  export {firebaseAuth, firebaseDB}
